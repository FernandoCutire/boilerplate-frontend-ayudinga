import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;

renderMethod(<App />, document.getElementById("root"));
