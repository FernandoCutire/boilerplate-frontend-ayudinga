import React from "react";

export const reduxComponent = ({
  count,
  handleIncrementClick,
  handleDecrementClick,
}) => (
  <div>
    <h1>Hola, bienvenido a este template con React y Redux! {count} </h1>
    <button onClick={handleDecrementClick}>Disminuir</button>
    <button onClick={handleIncrementClick}>Aumentar</button>
  </div>
);
