import React, {Component} from "react";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import {hot} from "react-hot-loader";
import styled, {css} from "styled-components";
import {Provider} from "react-redux";

import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import {store} from "../store/store";
import {ContainerRedux} from "./ContainerRedux";

import "../styles/App.css";

const Button = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0.5em 1em;
  padding: 0.25em 1em;

  ${(props) =>
          props.primary &&
          css`
            background: palevioletred;
            color: white;
          `}
`;

const Container = styled.div`
  text-align: center;
`;

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <h2>Bienvenido al boilerplate de React y Typescript</h2>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <ul className="navbar-nav mr-auto">
                            <li>
                                <Link to={"/"} className="nav-link">
                                    {" "}
                                    Home{" "}
                                </Link>
                            </li>
                            <li>
                                <Link to={"/contact"} className="nav-link">
                                    Contact
                                </Link>
                            </li>
                            <li>
                                <Link to={"/about"} className="nav-link">
                                    About
                                </Link>
                            </li>
                        </ul>
                    </nav>

                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/contact" component={Contact}/>
                        <Route path="/about" component={About}/>
                    </Switch>
                    <Container>
                        <Button>Styled</Button>
                        <Button primary>Components</Button>
                    </Container>
                    <Provider store={store}>
                        <ContainerRedux/>
                    </Provider>
                </div>
            </Router>
        );
    }
}

export default hot(module)(App);
