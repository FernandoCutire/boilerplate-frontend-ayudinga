import { connect } from "react-redux";
import { reduxComponent } from "./ReduxComponent";

const mapStateToProps = (state) => {
  return {
    count: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleIncrementClick: () => dispatch({ type: "INCREMENT" }),
    handleDecrementClick: () => dispatch({ type: "DECREMENT" }),
  };
};

export const ContainerRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(reduxComponent);
