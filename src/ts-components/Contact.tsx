import React from "react";

const Contact = () => (
    <div>
        <h2>Contact me</h2>
        <p>fernando@fernandocutire.com</p>
    </div>
)

export default Contact;
