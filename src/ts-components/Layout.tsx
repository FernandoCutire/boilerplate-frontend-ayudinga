import React from 'react'
import Footer from "./Footer";
import Header from "./Header";
import { SwitchProps} from "react-router-dom";

type Props = {
    children: SwitchProps
}


const Layout = ({children}: Props) => (
    <div>
        <Header/>
        {children}
        <Footer/>
    </div>
)

export default Layout