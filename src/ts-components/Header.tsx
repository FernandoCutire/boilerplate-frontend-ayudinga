import React from "react";
import {Link} from "react-router-dom";


const Header = () => (
    <>
        <h1>Bienvenido al React Boilerplate</h1>
        <div>
            <Link to="/">Home| </Link>
            <Link to="/about">About| </Link>
            <Link to="/contact">Contact</Link>
        </div>
    </>
)
export default Header