import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";

import Home from "../ts-components/Home";
import About from "../ts-components/About";
import Contact from "../ts-components/Contact";
import NotFound from "../ts-components/NotFound";
import Layout from "../ts-components/Layout";


const App = () => (
    <BrowserRouter>
        <Layout>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/about" component={About}/>
                <Route exact path="/contact" component={Contact}/>
                <Route component={NotFound}/>
            </Switch>
        </Layout>
    </BrowserRouter>
);

export default App