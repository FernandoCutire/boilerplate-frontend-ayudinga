import React from "react";
import ReactDOM from "react-dom";
import App from "./ts-routes/App";

ReactDOM.render(<App/>,document.getElementById("root"));

